<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class TaskController extends AbstractController
{
    /**
     * @Route("/tasks")
     */
    public function index()
    {
        $tasks = [
            ['id' => 1, 'name' => 'commissions', 'priority' => 1],
            ['id' => 2, 'name' => 'devoirs', 'priority' => 3],
            ['id' => 3, 'name' => 'nettoyage', 'priority' => 2]
          ];
        
          return $this->render('tasks.html.twig', [
            'tasks' => $tasks,
            'me' => 'Cédric'
        ]);
    }

    /**
     * @Route("/tasks/{id}")
     */
    public function detail($id)
    {
        return new Response('Détail de la tâche #' . $id);
    }
}